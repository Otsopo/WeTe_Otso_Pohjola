    <?php
    class Model {
        private $file = "messages.txt";
        private $nickname = "";

        public function messages() {
            if (file_exists($this->file)) {
                return file($this->file);
            } else {
                return array();
            }
        }

        public function add_message($message,$nickname) {
            $message = date("H:i:s: ")." ".$nickname."  " . $message;
            file_put_contents($this->file, "{$message}\n", FILE_APPEND);
            return $nickname;
        }
        
          public function add_nickname() {
              $nickstr = explode(" " , end(file($this->file)))[2];
              return $nickstr; }
    }
    ?>
