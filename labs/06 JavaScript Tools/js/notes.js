'use strict';
var notes = new Array();

function addItem() {
	var	textbox = document.getElementById('item');
	var itemText = textbox.value;
	textbox.value = '';
	textbox.focus();
	var i = 0;
	if(notes.length>0){
		
		var x = 0;
		for (i;i<notes.length;i++) {
			
		
			if(itemText==notes[i].title){
		
				notes[i].quantity++;
				notes[i] = {title: notes[i].title, quantity: notes[i].quantity};
				x = 1;
				
			}
		}
		if (x == 0){
			var newItem = {title: itemText, quantity: 1};
			notes.push(newItem);
		}
	}else{
		var newItem = {title: itemText, quantity: 1};
		notes.push(newItem);
	}
	
	displayList();


}

function displayList() {
	var table = document.getElementById('list');
	table.innerHTML = '';
	for (var i = 0; i<notes.length; i++) {
		var node = undefined;
		var note = notes[i];
		var node = document.createElement('tr');
		var html = '<td>'+note.title+'</td><td>'+note.quantity+'</td><td><a href="#" onClick="deleteIndex('+i+')">delete</td>';
		node.innerHTML = html;
		table.appendChild(node);
		
	}

		localStorage.notes = JSON.stringify(notes);
}

function deleteIndex(i) {
	notes.splice(i, 1);
	displayList();

}

var button = document.getElementById('add');
button.onclick = addItem;

function init() {
	 console.log('loadList');
	 if (localStorage.notes) {
		 notes = JSON.parse(localStorage.notes);
		 displayList();
	    }
}




window.onload = init;