<?php

include("diceclasses.inc.php");

$faces = $_GET["faces"];
$throws = $_GET["throws"];
$bias = $_GET["bias"];
$material = $_GET["material"];

$results = array();

if ($material !="" || $material != null){
    $dice = new PhysicalDice($faces,$bias,$material);
}else{
// make dice
$dice = new Dice($faces,$bias);
}
for ($i = 1; $i<=$throws; $i++) {
    $res = $dice->cast();
    $results[] = array('id' => strval($i), 'res' => strval($res));
}
$freqs = array();
for ($i = 1; $i<=$faces; $i++) {
    $freqs[] = array ('eyes' => strval($i), 'frequency' => strval($dice->getFreq($i)));
  
}
if ($material !="" || $material != null){
echo json_encode(array('faces'=>$faces,'results'=>$results,'frequencies'=>$freqs,'average'=>strval($dice->getAvg($freqs)),'material'=>$dice->getMaterial()));
}else{
    echo json_encode(array('faces'=>$faces,'results'=>$results,'frequencies'=>$freqs,'average'=>strval($dice->getAvg($freqs))));

}



?>