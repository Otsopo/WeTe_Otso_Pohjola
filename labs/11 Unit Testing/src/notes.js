var notes = (function () {
    var list =[];
    return{
        add: function(note) {
            if(note && !isWhite(note)){
                var item = {timestamp: Date.now(), text: note};
                list.push(item);
                return true;
        }
        return false;},
        
        remove: function(index) {
            if(list[index]){
                list.splice(index,1);
                return true;
            }
               return false;},
               
        count: function() {
            return list.length;
        },
        
        list: function() {
            return list;
        },
        find: function(str2){
            var array = [];
            var list = notes.list();
            if(str2){
                var str = str2.toLowerCase();
                 var len = notes.count(); 
                 for (var i = 0;i < len;i++){
                     var lowercaseNote = list[i].text.toLowerCase();
                     if(lowercaseNote.indexOf(str) != -1){
                         array.push(list[i]);
                         } 
                 }
                 
                 if (array.length == 0){
                     return false;
                 }
                 return array;
                         
            }
            return false;
        },
        clear: function(){
                list =[];
        }
    }
}());

function isWhite(note){
  if(note.trim().length == 0){
       return true;
   }else{
       return false;
   }
}