describe('notes module',function(){
      beforeEach(function() {
    notes.clear();
    notes.add('first note');
    notes.add('second note');
    notes.add('third note');
    notes.add('fourth note');
    notes.add('fifth note');
  });

    describe('adding a note', function() {
            it('should be able to add a new note', function () {
            expect(notes.add('sixth note')).toBe(true);
            expect(notes.count()).toBe(6);
            });
        it('should ignore blank notes', function() {
            expect(notes.add('')).toBe(false);
            expect(notes.count()).toBe(5);
             });
          it('should ignore notes containing only whitespace', function() {
            expect(notes.add('     ')).toBe(false);
            expect(notes.count()).toBe(5);
       
             });
        it('should require a string parameter',function() {
            expect(notes.add()).toBe(false);
            expect(notes.count()).toBe(5);
            
             });
        
    });
    
    describe('deleting a note', function() {
        it('should be able to delete the first index',function(){
            expect(notes.remove(0)).toBe(true);
            expect(notes.count()).toBe(4);
           
        });
        it('should be able to delete the last index',function() {
            expect(notes.remove(4)).toBe(true);
            expect(notes.count()).toBe(4);
 
        });
        it('should retur false if index is out of range',function(){
             expect(notes.remove(10)).toBe(false);
         expect(notes.count()).toBe(5);
 
        });
        
         
        
        it('should return fase is parametern is missing',function() {
             expect(notes.remove()).toBe(false);
        expect(notes.count()).toBe(5);

        });
       
    });
    
    describe('finding a note',function() {
       it('should find a note',function(){
           expect(notes.find('note').length).toBe(5);
       });
       it('should find a Note',function(){
           expect(notes.find('Note').length).toBe(5);
           
       });
       it('should find a th',function(){
           expect(notes.find('th').length).toBe(3);
           
       });
       it('should find a four',function(){
           expect(notes.find('four').length).toBe(1);
           
       });
       it('should find a six',function(){
           expect(notes.find('six')).toBe(false);
       });
       it('should return false if string is empty',function(){
           expect(notes.find('')).toBe(false);
           
       });
       it('should return false if parametern is missing',function(){
            expect(notes.find()).toBe(false);
           
       });
    });

  
});